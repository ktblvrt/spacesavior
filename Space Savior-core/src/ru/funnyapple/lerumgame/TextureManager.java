package ru.funnyapple.lerumgame;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class TextureManager {

	private TextureRegion[][] bulletTexture;
	private TextureRegion[][] enemyTexture;
	private TextureRegion[][] buttonTexture;

	private HashMap<String, TextureRegion> bulletAtlas = new HashMap<String, TextureRegion>();
	private HashMap<String, TextureRegion> enemyAtlas = new HashMap<String, TextureRegion>();
	private HashMap<String, TextureRegion> buttonAtlas = new HashMap<String, TextureRegion>();

	private Texture messageTexture;

	public TextureManager() {
		bulletTexture = TextureRegion.split(new Texture(Gdx.files.internal("bullets.png")), 16, 30);
		enemyTexture = TextureRegion.split(new Texture(Gdx.files.internal("enemies.png")), 78, 50);
		buttonTexture = TextureRegion.split(new Texture(Gdx.files.internal("startbutton.png")), 439, 119);

		bulletAtlas.put("playerbullet", bulletTexture[0][1]);
		enemyAtlas.put("enemy", enemyTexture[0][1]);
		enemyAtlas.put("snake", enemyTexture[0][2]);
		buttonAtlas.put("start0", buttonTexture[0][1]);
		buttonAtlas.put("start1", buttonTexture[0][0]);

		messageTexture = new Texture(Gdx.files.internal("message.png"));
	}

	public HashMap<String, TextureRegion> getBulletAtlas() {
		return bulletAtlas;
	}

	public HashMap<String, TextureRegion> getEnemyAtlas() {
		return enemyAtlas;
	}

	public HashMap<String, TextureRegion> getButtonAtlas() {
		return buttonAtlas;
	}

	public Texture getMessageTexture() {
		return messageTexture;
	}

}
