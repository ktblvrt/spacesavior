package ru.funnyapple.lerumgame;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;

import ru.funnyapple.lerumgame.screens.GameScreen;
import ru.funnyapple.lerumgame.screens.MenuScreen;

public class Main extends Game {

	public static final int W = 800;
	public static final int H = 480;

	private static Main instance;
	private TextureManager textureManager;
	private Screen gameScreen;
	private Screen menuScreen;

	public Main() {
		instance = this;

		gameScreen = new GameScreen(this);
		menuScreen = new MenuScreen();
	}

	@Override
	public void create() {
		textureManager = new TextureManager();

		setScreen(menuScreen);
		// setScreen(gameScreen);
	}

	public static Main get() {
		return instance;
	}

	@Override
	public void render() {
		super.render();
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	public TextureManager getTextureManager() {
		return textureManager;
	}

	public MenuScreen getMenuScreen() {
		return (MenuScreen) menuScreen;
	}

	public Screen getGameScreen(Main main) {
		return gameScreen;
	}

}
