package ru.funnyapple.lerumgame.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import ru.funnyapple.lerumgame.Main;
import ru.funnyapple.lerumgame.gui.MenuButton;

public class MenuScreen implements Screen {

	private Stage menu;
	private StretchViewport viewport;
	private Actor pressed;
	private OrthographicCamera cam;

	private MenuButton startButton;
	
	@Override
	public void show() {
		cam = new OrthographicCamera();
		cam.setToOrtho(false);
		viewport = new StretchViewport(Main.W, Main.H, cam);
		menu = new Stage(viewport);
		startButton = new MenuButton(200, 200, 
				Main.get().getTextureManager().getButtonAtlas().get("start0"),
				Main.get().getTextureManager().getButtonAtlas().get("start1"));

		menu.addListener(new InputListener() {
			public boolean touchDown(InputEvent e, float x, float y, int pointer, int button) {
				pressed = menu.hit(x, y, true);
				return false;
			}
			/*public void touchUp(InputEvent e, float x, float y, int pointer, int button) {
				pressed = null;                                                                           *CRYING ON JAVA*
			}*/
			/*public boolean touchDown(InputEvent e, float x, float y, int pointer, int button) {
				Actor hit = menu.hit(x, y, true);
				if (hit instanceof MenuButton) {
					if (x >= getX() && x <= getX() + getWidth() && y > getY() && y < getY() + getHeight()) {
					((MenuButton) hit).setPressed();
				}
				return true;
			}
			 public void touchUp(InputEvent e, float x, float y, int pointer, int button) {
				pressed = menu.hit(x, y, true);
				if (pressed instanceof MenuButton) {
					((MenuButton) pressed).setReleased();
				}
			}*/
		});

		Gdx.input.setInputProcessor(menu);

		menu.addActor(startButton);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		menu.act(delta);
		menu.draw();
	}

	public Stage getStage() {
		return menu;
	}

	public Actor getPressed() {
		return pressed;
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		menu.dispose();
	}

}
