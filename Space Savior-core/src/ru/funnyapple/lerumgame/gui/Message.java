package ru.funnyapple.lerumgame.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;

import ru.funnyapple.lerumgame.FontGenerator;
import ru.funnyapple.lerumgame.GameManager;
import ru.funnyapple.lerumgame.Main;
import ru.funnyapple.lerumgame.screens.GameScreen;

public class Message extends Actor {

	private Texture t;
	private BitmapFont font;
	private String text;
	private float mx;
	private float my;

	public Message(String text) {

		if (GameManager.hasMsg()) {
			delete();
			return;
		}

		t = Main.get().getTextureManager().getMessageTexture();
		font = FontGenerator.gameFont();
		this.text = text;

		setWidth(t.getWidth());
		setHeight(t.getHeight());

		mx = GameScreen.get().getRightBorder() / 2 - getWidth() / 2;
		my = GameScreen.get().getH() / 2 - getHeight() / 2;

		setX(mx);
		setY(my);

		GameScreen.get().getStage().addActor(this);
		GameScreen.get().setPaused(true);
		GameManager.setMsg(true);
	}

	@Override
	public void draw(Batch batch, float alpha) {

		if (Gdx.input.isTouched()) {
			float x = Gdx.input.getX();
			float y = Gdx.input.getY();

			if (x >= getX() && x <= getX() + getWidth() && y > getY() && y < getY() + getHeight()) {
				GameScreen.get().setPaused(false);

				if (text.contains("Game Over")) {
					GameManager.lose();
				}

				delete();
			}
		}

		batch.draw(t, getX(), getY(), getWidth(), getHeight());
		font.draw(batch, text, mx + getWidth() / 5, my + getHeight() / 2);
	}

	public String getText() {
		return text;
	}

	public void delete() {
		GameManager.setMsg(false);
		remove();
		if (font != null) font.dispose();
		clear();
	}

}
