package ru.funnyapple.lerumgame.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class RightBlock extends Actor {

	private Texture t;

	public RightBlock() {
		t = new Texture(Gdx.files.internal("rightblock.png"));

		setX(700);
		setY(0);
		setWidth(t.getWidth());
		setHeight(t.getHeight());
	}

	@Override
	public void draw(Batch batch, float alpha) {
		batch.draw(t, getX(), getY(), getWidth(), getHeight());
	}

}
