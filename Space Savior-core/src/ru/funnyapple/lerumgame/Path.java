package ru.funnyapple.lerumgame;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.Vector2;

public class Path {
	
	private List<Vector2> path;
	
	public Path() {
		path = new ArrayList<Vector2>();
	}
	
	public void addWaypoint(Vector2... point) {
		for (Vector2 p: point) {
			path.add(p);
		}
	}
	
	public void removeLastPoint() {
		path.remove(0);
	}
	
	public List<Vector2> list() {
		return path;
	}
	
	public Vector2 nextPoint() {
		return path.get(0);
	}
	
	public boolean isEnd() {
		return path.isEmpty();
	}
	
}
