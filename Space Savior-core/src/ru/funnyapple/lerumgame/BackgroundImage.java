package ru.funnyapple.lerumgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class BackgroundImage extends Image {
	
	private Texture t = new Texture(Gdx.files.internal("background1.png"));
	
	//������ �����. ���� ������� 2
	
	public BackgroundImage() {
		setX(0);
		setY(0);
		setWidth(t.getWidth());
		setHeight(t.getHeight());
	}
	
	@Override
	public void draw(Batch b, float a) {
		b.draw(t, getX(), getY(), getWidth(), getHeight());
	}
	
}
