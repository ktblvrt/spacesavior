package ru.funnyapple.lerumgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

public class FontGenerator {
	
	//��� � ��� ��������� ������
	
	//���� ������
	private static final float R = 0.400f;
	private static final float G = 0.804f;
	private static final float B = 0.667f;
	
	private static final int SIZE = 16;
	private static final float BORDER = 0.6f;
	
	private static FreeTypeFontGenerator gen;
	private static FreeTypeFontParameter param;
	
	public static BitmapFont gameFont() {
		gen = new FreeTypeFontGenerator(Gdx.files.internal("GOST_B_.ttf"));
		param = new FreeTypeFontParameter();
		param.size = SIZE;
		param.color = Color.CYAN;
		param.borderColor = new Color(R, G, B, 1);
		param.borderWidth = BORDER;
		BitmapFont font = gen.generateFont(param);
		gen.dispose();
		return font;
	}
	
}
