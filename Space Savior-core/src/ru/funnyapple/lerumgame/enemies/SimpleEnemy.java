package ru.funnyapple.lerumgame.enemies;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;

import ru.funnyapple.lerumgame.GameManager;
import ru.funnyapple.lerumgame.Main;
import ru.funnyapple.lerumgame.gui.Message;

public class SimpleEnemy extends Enemy {
	
	private final float MIN_SPEED = 30f;
	private final float coef = 0.2f;
	
	private static TextureRegion texture = Main.get().getTextureManager().getEnemyAtlas().get("enemy");
	
	public SimpleEnemy(float x, float y) {
		super(texture, x, y);
		setSpeed(MIN_SPEED + MathUtils.random(GameManager.getScore() * coef));
	}
	
	public void listenBehavior() {
		if (getY() < -getWidth()) {
			new Message("Game Over. Score: " + GameManager.getScore());
			die();
		}
	}

}
