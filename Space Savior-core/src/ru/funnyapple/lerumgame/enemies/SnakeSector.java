package ru.funnyapple.lerumgame.enemies;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;

import ru.funnyapple.lerumgame.Main;
import ru.funnyapple.lerumgame.Path;
import ru.funnyapple.lerumgame.Utils;

public class SnakeSector extends Enemy {
	
	private static TextureRegion texture = Main.get().getTextureManager().getEnemyAtlas().get("snake");
	
	private final float SPEED = 40f;
	
	private float angle;
	private Path path;
	
	public SnakeSector(float x, float y) {
		super(texture, x, y);
		
		setHealth(2);
		setPrice(2);
		setOrigin(Align.center);
	}
	
	public void listenBehavior() {
		if (!path.isEnd()) {
			moveTo(path.nextPoint());
		} else {
			die();
		}
	}
	
	private void moveTo(Vector2 point) {
		
		angle = (float) Math.atan2(point.y - getY(), point.x - getX()) * MathUtils.radDeg + 90;
		setRotation(angle);
		
		float x = (float) Math.cos(angle) * SPEED * Gdx.graphics.getDeltaTime();
		float y = (float) Math.sin(angle) * SPEED * Gdx.graphics.getDeltaTime();
		
		setPosition(getX() + x, getY() + y);
		
		if (Utils.inRange(getX(), point.x - 50, point.x + 50) &&
				Utils.inRange(getX(), point.y - 50, point.y + 50)) {
			path.removeLastPoint();
		}
		
	}
	
	public void setPath(Path path) {
		this.path = path;
	}
	
}
